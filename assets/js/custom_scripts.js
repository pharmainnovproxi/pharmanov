document.addEventListener("DOMContentLoaded", function(){
    window.addEventListener('scroll', function() {
        if (window.scrollY > 10) {
          document.getElementById('main_nav_bar').style.display= "none";
          document.getElementById('main_nav_bar').style.opacity= "0";


          // add padding top to show content behind navbar
  
        } else {
            document.getElementById('main_nav_bar').style.display = "block";
          document.getElementById('main_nav_bar').style.opacity= "1";


          } 
    });
  }); 